package com.cantor.mo.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.test1;


@Component("ctmInBoundService")
public class CtmInBoundService implements ExceptionListener {

	private CtmOutBoundService ctmOutBoundService;
	
	private final CtmBlockDaoImpl ctmBlockDaoImpl;
	private final CtmAllocDaoImpl ctmAllocationDaoImpl;

	private DataDictionary dd;

	@Autowired
	public CtmInBoundService(CtmBlockDaoImpl blockDaoImpl, CtmAllocDaoImpl ctmAllocationDaoImpl,
			Environment environment, CAMOMailSender camoMailSender, JmsTemplate guiOutboundTopicTemplate,
			SymbolDaoImpl symbolDao, LoadStaticDataUtils loadStaticDataUtils, CtmOutBoundService ctmOutBoundService) {
		this.ctmBlockDaoImpl = blockDaoImpl;
		this.ctmAllocationDaoImpl = ctmAllocationDaoImpl;
		this.camoMailSender = camoMailSender;
		this.environment = environment;
		this.guiOutboundTopicTemplate = guiOutboundTopicTemplate;
		this.symbolDao = symbolDao;
		this.loadStaticDataUtils = loadStaticDataUtils;
		
		this.ctmOutBoundService = ctmOutBoundService;
		
		initJms();
		
		subAccountMap= loadStaticDataUtils.getSubAcctsMapByCtmGlobalCodes();

		try {
			dd = new DataDictionary("fix44_ctm.xml");
			dd.setCheckFieldsOutOfOrder(false);
			dd.setCheckUserDefinedFields(false);
			dd.setCheckUnorderedGroupFields(false);
			dd.setCheckFieldsHaveValues(false);
			//dd.setAllowUnknownMessageFields(true);
			
		} catch (ConfigError e) {
			logger.error("ConfigError:"+e,e);
		}
	}

	private void initJms() {

		logger.info("Entering initJms ...");

		try {
			TopicConnectionFactory incoming_factory = new com.tibco.tibjms.TibjmsTopicConnectionFactory(
					environment.getRequiredProperty("jms.req.serverUrl"));

			incoming_connection = incoming_factory.createTopicConnection(
					environment.getRequiredProperty("jms.req.userName"),
					environment.getRequiredProperty("jms.req.password"));

			incoming_session = incoming_connection.createTopicSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
			incoming_topic = incoming_session
					.createTopic(environment.getRequiredProperty("jms.req.ctmSubscriberTopic"));

			incoming_connection.setExceptionListener(this);

			incoming_subscriber = incoming_session.createDurableSubscriber(incoming_topic,
								environment.getRequiredProperty("jms.req.ctmDurableName"), null, false);

			//incoming_subscriber = incoming_session.createSubscriber(incoming_topic);

			logger.info("Subscribing:" + incoming_topic.getTopicName() + "-");

			incoming_subscriber.setMessageListener(new MessageListener() {

				public void onMessage(javax.jms.Message msg) {

					logger.debug("In to ctmInBoundService onMessage ");

					try {

						TibjmsMapMessage map = (TibjmsMapMessage) msg;

						String msgType = map.getString("MessageType");
						String fixStr = map.getString("EventData");
						
						if ("0".equals(msgType)) {
							return;
						}

						// String msgType = msg.getStringProperty("MessageType");
						// String message = msg.getStringProperty("EventData");

						logger.info(" Received message type :" + msgType+"\n"+fixStr);

						switch (msgType) {

						case MsgTypes.CTM_AE_MESSAGE:
							processAEMessage(fixStr);
							break;
						case MsgTypes.CTM_AR_MESSAGE:
							processARMessage(fixStr);
							break;
						case MsgTypes.CTM_J_MESSAGE:
							processJMessage(fixStr);
							break;
						case MsgTypes.CTM_AK_MESSAGE:
							processAKMessage(fixStr);
							break;
						case MsgTypes.CTM_AU_MESSAGE:
							processAUMessage(fixStr);
							break;
						case MsgTypes.CTM_j_MESSAGE:
							processjMessage(fixStr,MsgTypes.CTM_j_MESSAGE);
							break;
						case MsgTypes.CTM_3_MESSAGE:
							processjMessage(fixStr,MsgTypes.CTM_3_MESSAGE);
							break;
						}

					} catch (Exception exception) {
						camoMailSender.sendMail("Error processing the jms message",
								"Error processing the jms message in ctmInBoundService " + "\n" + exception);
						logger.error("Error processing the jms message", exception);
					}
					logger.debug("Exiting ctmInBoundService onMessage ");
				}
			});

			Thread t = new Thread(() -> {
				
				try {
					logger.info("incoming_connection start");
					Thread.sleep(10000);
					incoming_connection.start();
					logger.info("incoming_connection end");
				} catch (JMSException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			t.start();
			

		} catch (IllegalStateException e) {
			logger.error("IllegalStateException:" + e, e);
		} catch (JMSException e) {
			logger.error("JMSException:" + e, e);
	
		}

		logger.info("Exiting initJms ...");

	}

	public void processAEMessage(String message) {
		processCtmAEMessage(message);
	}

	public void processARMessage(String message) {

		processCtmBlockResponse(message);

	}

	public void processJMessage(String message) {
		processCtmAllocation(message);
	}
	
	public void processjMessage(String message, String type) {
		processCtmBusinessReject(message, type);
	}

}
